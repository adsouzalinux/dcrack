<?php

include("dbcon.php");


?>

<?php   include("header.php");  ?>



	<form action="" method="post">
		<!-- ============================== Fieldset 1 ============================== -->
		<fieldset>
			<legend>Lorem ipsum:</legend>
				<label for="input-one" class="float"><strong>Input One:</strong></label><br />
				<input class="inp-text" name="input-one-name" id="input-one" type="text" size="30" /><br />

				<label for="input-two" class="float"><strong>Input Two:</strong></label><br />
				<input class="inp-text" name="input-two-name"  id="input-two" type="text" size="30" />
		</fieldset>
		<!-- ============================== Fieldset 1 end ============================== -->


		<!-- ============================== Fieldset 2 ============================== -->
		<fieldset>
			<legend>Praesent vitae:</legend>
				<label for="option1"><input class="choose" name="option[]" id="option1" type="checkbox" value="1" /> &nbsp; Pellentesque fermentum tellus</label><br />
				<label for="option2"><input class="choose" name="option[]" id="option2" type="checkbox" value="2" /> &nbsp; Maecenas at tellus</label><br />
				<label for="option3"><input class="choose" name="option[]" id="option3" type="checkbox" value="3" /> &nbsp; Curabitur tincidunt nisl eget</label><br />
				<label for="option4"><input class="choose" name="option[]" id="option4" type="checkbox" value="4" /> &nbsp; Curabitur quis orci vitae velit facilisis</label><br />
				<label for="option5"><input class="choose" name="option[]" id="option5" type="checkbox" value="5" /> &nbsp; Class aptent taciti sociosqu ad litora</label>
		</fieldset>
		<!-- ============================== Fieldset 2 end ============================== -->


		<!-- ============================== Fieldset 3 ============================== -->
		<fieldset>
			<legend>Fusce odio:</legend>
			<textarea name="textarea-name" id="message" cols="30" rows="5" title="Note or message"></textarea><br />
			<label class="spam-protection" for="protection">Spam protection: <strong>6&nbsp;+&nbsp;5&nbsp;=</strong></label><br />
			<input class="answer" type="text" name="antispam" id="protection" size="5" />
		</fieldset>
		<!-- ============================== Fieldset 3 end ============================== -->

		<p><input class="submit-button" type="submit" alt="SUBMIT" name="Submit" value="SUBMIT" /></p>
	</form>
<?php include("footer.php"); ?>
