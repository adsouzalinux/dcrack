
<script>
window.onload=function(){
  var tfrow = document.getElementById('tfhover').rows.length;
  var tbRow=[];
  for (var i=1;i<tfrow;i++) {
    tbRow[i]=document.getElementById('tfhover').rows[i];
    tbRow[i].onmouseover = function(){
      this.style.backgroundColor = '#ffffff';
    };
    tbRow[i].onmouseout = function() {
      this.style.backgroundColor = '#d4e3e5';
    };
  }
};


//--------------------------------

function pop(Server_Id,Div_Id){
var server_id = Server_Id ; 
var div_id = Div_Id ; 
var datastr = 'moreinfo='+ Server_Id; 

$.ajax({
    type        : "POST",
    cache       : false,
    async       : false,
    url         : "get_server_details.php",
    data        : datastr,
    success: function(data){
document.getElementById(div_id).style.display='inline';
$('.parentDisable').html(data);
return false
//alert(data);
  
       }
    });
}


function enabledisablenext(element_id,input_id){ 

var ID=element_id;
var input_ID=input_id ;

var elim= document.getElementById(input_ID);
 $('#'+input_ID).change(function(){
        if (elim.checked) {
            $('#'+input_ID).attr("checked", "checked");
            document.getElementById(ID).style.display='inline';
        }
        else {
      document.getElementById(ID).style.display='none';
            $('#'+input_ID).removeAttr("checked");
        }
    });




} 







function pop_edit(Server_Id,Div_Id){
var server_id = Server_Id ;
var div_id = Div_Id ;
var datastr = 'editserverid='+ Server_Id;

$.ajax({
    type        : "POST",
    cache       : false,
    async       : false,
    url         : "edit_server_details.php",
    data        : datastr,
    success: function(data){
document.getElementById(div_id).style.display='inline';
$('.parentDisable').html(data);
return false
//alert(data);
  
       }
    });



}




function formSubmit()
{
document.getElementById("form_edit_server").submit();
}






function hide(div){
document.getElementById(div).style.display='none';
return false
}


/*

onselect of Domu , a dropdown appears 
with all Dom0 servers 

*/
$(document).ready(function(){
$(".DOM").change(function(){
var dom=$(this).val();
var dataString = 'Dom='+ dom;

$.ajax({
type: "POST",
url: "get_all_Dom0.php",
data: dataString,
cache: false,
success: function(html){
$("#alldom0").html(html);
      }
    });
  });
});




// this is to toggle on and off the forms and details page 
 
$(document).ready(function(){
  $("legend#rackinput").click(function(){
  $("#rackinput_colapse").toggle();
  });
  $("fieldset.fieldsetcol1 legend#rack").click(function(){
  $("table#tfhover").toggle();
  });

  $("fieldset.fieldsetcol1 legend#server").click(function(){
  $("table#serverhover").toggle();
  });

});



function validateP(){  
$('#errorlable1').empty();
  var validator = $("#form1").validate({
    rules: {                      
       Rname :"required",
       Rroom:  {
      required: true,
      number: true
      },
       Rsize:  {
      required: true,
      number: true
      },
       Rnotes: "required" 
      },                           
      errorLabelContainer: "#errorlable1",   
      messages: {
      Rname :" Enter Rack name ",
      Rroom: "Enter Room Number ",
      Rsize: "Enter number of servers in rack",
      Rnotes: "Enter additional notes  " 
      }
  });
  
if ($('#form1').valid())
    $('#form1').submit();

}


function validateF2(){  
$('#errorlable2').empty();
  var validator = $("#form2").validate({
    rules: {                      
      DOM:"required",
       Sname:  {
      required: true
      },
       Sappname:  {
      required: true
      },
      Sipaddress:  {
      required: true
      },
       SMemory:"required", 
       Sdisksize:"required", 
       SAddnotes:"required" 
      },                           
      errorLabelContainer: "#errorlable2",   
      messages: {
      DOM:" Enter Rack name ",
      Sname: "Enter Room Number ",
      Sappname: "Enter number of servers in rack",
      SMemory: "Enter additional notes  ", 
      Sdisksize: "Enter additional notes  ", 
      SAddnotes: "Enter additional notes  ", 
      Sipaddress: "Enter additional notes  " 

      }
  });
  
if ($('#form2').valid())
    $('#form2').submit();

}


function del_server(row_id,dom){
var ID = row_id;
var DOM = dom;

  if(confirm('Are you sure you want to delete?')){
  var server_ip = document.getElementById("server_ip_"+ID).innerHTML ;


    var datastr = 'SEDIT=DEL&Sid='+ ID +'&server_ip='+ server_ip +'&DOM='+ DOM ;

    $.ajax({
    type        : "POST",
    cache       : false,
    dataType    : "json",  
    async       : false,
    url         : "delete_server.php",
    data        : datastr,
    success: function(data){
      
      if ( trim(data.Sconfirm) == "Y" ){
      $('#tr_server_'+ ID).remove();
     alert(data.message);
      } else if ( trim(data.Sconfirm) == "N" ) { 
      alert(data.message);
         }
      }
    });
  }
}


// ------this JS is for rack details ------ 

var array_rack = ["#rname", "#rroom", "#rsize","#rnotes"];
//var data221 = '535';
// ----------------------------
function del_edit_Me(edit_del,row_id){
var ID=row_id;
var rname_input= document.getElementById('rname_'+ID).innerHTML ;
var rroom_input= document.getElementById("rroom_"+ID).innerHTML ;
var rsize_input= document.getElementById("rsize_"+ID).innerHTML ;
var rnotes_input= document.getElementById("rnotes_"+ID).innerHTML ;

if ( edit_del == 'del' ) { 
  if(confirm('Are you sure you want to delete?')){
  var rname_input= document.getElementById('rname_'+ID).innerHTML ;
  var rroom_input= document.getElementById("rroom_"+ID).innerHTML ;
  var rsize_input= document.getElementById("rsize_"+ID).innerHTML ;
  var rnotes_input= document.getElementById("rnotes_"+ID).innerHTML ;


    var dataString = 'REDIT=DEL&Rid='+ ID +'&Rname='+ rname_input +'&Rroom='+ rroom_input +'&Rsize='+ rsize_input +'&Rnotes='+ rnotes_input;
   var data22 =  ajaxcall(dataString);
      if ( trim(data22) == "Y" ){
      $('.main_edit_tr#'+ ID).remove();
      }
   }
 }else if( edit_del == 'edit' ) {
    for (var i in array_rack) {
      $(array_rack[i]+"_"+ID).hide();
      $(array_rack[i]+"_input_"+ID).show();
    }
    $('#edit_td_'+ ID).hide() ;    
    $('#save_td_'+ ID).show() ;  


 } else if (  edit_del == 'save') {   
    var rname_input= document.getElementById('rname_input_'+ID).value ;
    var rroom_input= document.getElementById("rroom_input_"+ID).value ;
    var rsize_input= document.getElementById("rsize_input_"+ID).value ;
    var rnotes_input= document.getElementById("rnotes_input_"+ID).value ;

    var dataString = 'REDIT=EDIT&Rid='+ ID +'&Rname='+ rname_input +'&Rroom='+ rroom_input +'&Rsize='+ rsize_input +'&Rnotes='+ rnotes_input;
   var  data221 = ajaxcall(dataString);
        
    if ( trim(data221) == 'Y' ){
       $("#rname_"+ID).html(rname_input); 
       $("#rroom_"+ID).html(rroom_input); 
       $("#rsize_"+ID).html(rsize_input); 
       $("#rnotes_"+ID).html(rnotes_input); 
      for (var i in array_rack) {
      $(array_rack[i]+"_"+ID).show();
      $(array_rack[i]+"_input_"+ID).hide();
      }
      $('#edit_td_'+ ID).show() ;
      $('#save_td_'+ ID).hide() ;

  }
 } 

}

function ajaxcall ( datastr) {
var myret = '' ;
$.ajax({
    type        : "POST",
    cache       : false,
    async       : false,
    url         : "edit_del.php",
    data        : datastr,
    success: function(data){
      myret = data ;
       }
    });

return myret;

}


//------------------------------

function trim(str) {
        return str.replace(/^\s+|\s+$/g,"");
}



/*

*/









</script>
