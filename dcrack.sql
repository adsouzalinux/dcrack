-- MySQL dump 10.11
--
-- Host: localhost    Database: dcrack
-- ------------------------------------------------------
-- Server version	5.0.77

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `login` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `emailid` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'agnello','agnello.dsouza@gmail.com','qwerasdf'),(2,'ganesh','ganesh@corp.india.com','aaaaa');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `racks`
--

DROP TABLE IF EXISTS `racks`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `racks` (
  `id` int(11) NOT NULL auto_increment,
  `rname` varchar(50) NOT NULL,
  `rsize` int(11) NOT NULL,
  `rroom` varchar(50) NOT NULL,
  `rnotes` varchar(255) NOT NULL,
  PRIMARY KEY  (`rname`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `racks`
--

LOCK TABLES `racks` WRITE;
/*!40000 ALTER TABLE `racks` DISABLE KEYS */;
INSERT INTO `racks` VALUES (1,'rack1',22,'101','servers in rack 12'),(2,'rack2',22,'101','servers in rack 2'),(9,'aws',100,'100','All server in AWS');
/*!40000 ALTER TABLE `racks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `server_assgn_grp`
--

DROP TABLE IF EXISTS `server_assgn_grp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `server_assgn_grp` (
  `id` int(11) NOT NULL auto_increment,
  `server_id` int(11) NOT NULL,
  `server_type_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `server_assgn_grp`
--

LOCK TABLES `server_assgn_grp` WRITE;
/*!40000 ALTER TABLE `server_assgn_grp` DISABLE KEYS */;
INSERT INTO `server_assgn_grp` VALUES (1,13,1),(2,14,4),(3,15,4),(4,16,4),(5,17,4);
/*!40000 ALTER TABLE `server_assgn_grp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servergrouplist`
--

DROP TABLE IF EXISTS `servergrouplist`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `servergrouplist` (
  `id` int(11) NOT NULL auto_increment,
  `server_type` varchar(25) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `servergrouplist`
--

LOCK TABLES `servergrouplist` WRITE;
/*!40000 ALTER TABLE `servergrouplist` DISABLE KEYS */;
INSERT INTO `servergrouplist` VALUES (1,'websites'),(2,'database'),(3,'cache'),(4,'others');
/*!40000 ALTER TABLE `servergrouplist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `servers` (
  `id` int(11) NOT NULL auto_increment,
  `rack_id` int(11) NOT NULL,
  `ip_addr` varchar(50) NOT NULL,
  `Dom` enum('dom0','domU') NOT NULL,
  `server_name` varchar(100) NOT NULL,
  `memory` varchar(100) NOT NULL,
  `disksize` varchar(255) NOT NULL,
  `websites` varchar(255) default NULL,
  `serv_DB` varchar(255) default NULL,
  `cache` varchar(255) default NULL,
  `others` varchar(255) default NULL,
  `servernotes` varchar(255) default NULL,
  `ipaddrofdom0` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `servers`
--

LOCK TABLES `servers` WRITE;
/*!40000 ALTER TABLE `servers` DISABLE KEYS */;
INSERT INTO `servers` VALUES (13,1,'192.168.1.150','dom0','server150','5gb','sdfsdf','india.com','','','','sdfsdf',''),(14,1,'192.168.1.151','dom0','server151','5gb','sdfsdf','','','','others','sdfs',''),(15,1,'192.168.1.152','dom0','server152','5gb','sfsdfd','','','','this is DOM0','sfsdf',''),(16,1,'192.168.1.153','dom0','server153','34gb','ssdfsdf','','','','this is DOM0','sdfsdf',''),(17,1,'192.168.1.154','dom0','server154','34gb','sdfsdf','','','','this is DOM0','sdfsdf','');
/*!40000 ALTER TABLE `servers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-14 13:15:32
