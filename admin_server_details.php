 <!-- ============================== Fieldset 2 ============================== -->
<?php 
$Sel_all_Dom0_servers  = $con->selectFrom($table = "servers", $columns = null , $where = array('Dom' => 'dom0') , $like = null , $orderby = null, $direction = null, $limit = null , $offset = null );



if ( $disply_error_editserver ) { 
  echo '<legend class="server_error">'. $disply_error_editserver  .' </legend>';
}

?>
          

 
          <fieldset class="fieldsetcol1">
            <legend id="server">server  Details </legend>
            <table id="serverhover" class="servertable" border="1">
              <tr><th>Number</th><th>ipaddress</th><th>rack number</th><th>DOM</th><th>server Name</th><th>additional info</th><th>Edit/Del</th></tr>
      

               <?php foreach ($Sel_all_Dom0_servers['result'] as $res  ){  ?>

                  <?php $server_num++; ?>


             <tr id ="tr_server_<?php echo $res['id']  ?>" class="main_edit_tr_server_DOM0" >

                <td class="edit_server_Num_td" >
                   <span ><?php echo $server_num ; ?></span>
                </td>
                
                <td class="edit_server_ip_td">
                 <span id="server_ip_<?php echo $res['id']; ?>" class="text"><?php echo $res['ip_addr']; ?></span>
                 <input type="text" value="<?php echo  $res['ip_addr']; ?>" class="editbox" id="server_ip_input_<?php echo $res['id'] ; ?>" >
                </td>

                <td class="edit_server_rack_num">
                 <span id="server_rack_<?php echo $res['id']; ?>" class="text"><?php echo "rack ".$res['rack_id']; ?></span>
                 <input type="text" value="<?php echo  $res['rack_id']; ?>" class="editbox" id="server_rack_<?php echo $res['id'] ; ?>" >
                </td>
                
                <td class="edit_DOM">
                 <span id="DOM_<?php echo $res['id']; ?>" class="text"><?php echo $res['Dom']; ?></span>
                 <input type="text" value="<?php echo  $res['Dom']; ?>" class="editbox" id="DOM_input_<?php echo $res['id'] ; ?>" >
                </td>
                
                <td class="edit_server_name">
                 <span id="server_name_<?php echo $res['id']; ?>" class="text"><?php echo $res['server_name']; ?></span>
                 <input type="text" value="<?php echo  $res['server_name']; ?>" class="editbox" id="server_name_input_<?php echo $res['id'] ; ?>" >
                </td>
              
                
                <td class="edit_additional_info">
                  <a href="#" onClick="return pop('<?php echo $res['id']; ?>','pop')"><img src="images/more.png"  height="28" width="28"></a>
                </td>


                <td>
                <a  id="edit_td_server_<?php echo $res['id'] ; ?>" onclick="return pop_edit('<?php echo $res['id']; ?>','pop')"  href="#" >
                  <img src="images/edit.png"  height="28" width="28">
                </a>
                
                <a  id="del_td_server_<?php  echo $res['id'] ; ?>" onclick="del_server('<?php echo $res['id'] ; ?>','<?php echo $res['Dom'] ; ?>' )"      href="#" >
                  <img src="images/delete.png"  height="28" width="28">
                </a>
                </td>
               
            </tr>

<?php
$Sel_all_DomU_servers  = $con->selectFrom($table = "servers", $columns = null , $where = array('Dom' => 'domU','ipaddrofdom0' =>  $res['ip_addr']) , $like = null , $orderby = null, $direction = null, $limit = null , $offset = null );
unset($res);
$inner_row_cnt= 1;

?>
<!-- ============================================================================================ --> 
                  

             <div class="inner_row_<?php echo $server_num ?>">
          <?php foreach ($Sel_all_DomU_servers['result'] as $res  ){  ?>


              <tr id ="tr_server_<?php echo $res['id']  ?>" class="main_edit_tr_server_DOMU" >

                <td class="edit_server_Num_td" >
                   <span ><?php echo $inner_row_cnt++ ; ?></span>
                </td>
                
                <td class="edit_server_ip_td">
                 <span id="server_ip_<?php echo $res['id']; ?>" class="text"><?php echo $res['ip_addr']; ?></span>
                 <input type="text" value="<?php echo  $res['ip_addr']; ?>" class="editbox" id="server_ip_input_<?php echo $res['id'] ; ?>" >
                </td>

                <td class="edit_server_rack_num">
                 <span id="server_rack_<?php echo $res['id']; ?>" class="text"><?php echo "rack ".$res['rack_id']; ?></span>
                 <input type="text" value="<?php echo  $res['rack_id']; ?>" class="editbox" id="server_rack_<?php echo $res['id'] ; ?>" >
                </td>
                
                <td class="edit_DOM">
                 <span id="DOM_<?php echo $res['id']; ?>" class="text"><?php echo $res['Dom']; ?></span>
                 <input type="text" value="<?php echo  $res['Dom']; ?>" class="editbox" id="DOM_input_<?php echo $res['id'] ; ?>" >
                </td>
                
                <td class="edit_server_name">
                 <span id="server_name_<?php echo $res['id']; ?>" class="text"><?php echo $res['server_name']; ?></span>
                 <input type="text" value="<?php echo  $res['server_name']; ?>" class="editbox" id="server_name_input_<?php echo $res['id'] ; ?>" >
                </td>
              
                
                <td class="edit_additional_info">
                  <a href="#" onClick="return pop('<?php echo $res['id']; ?>','pop')"><img src="images/more.png"  height="28" width="28"></a>
                </td>


                <td>
                <a  id="edit_td_server_<?php echo $res['id'] ; ?>" onclick="return pop_edit('<?php echo $res['id']; ?>','pop')"  href="#" ><img src="images/edit.png"  height="28" width="28"></a>
                <a  id="del_td_server_<?php  echo $res['id'] ; ?>" onclick="del_server('<?php echo $res['id'] ; ?>','<?php echo $res['Dom'] ; ?>' )"    href="#" ><img src="images/delete.png"  height="28" width="28"></a>
                </td>

            </tr>

                <?php  } ?>

           </div> 

                <?php  
                unset($inner_row_cnt) ;
                ?>
<!-- ============================================================================================ --> 



                <?php  } ?>


            </table>

          </fieldset>
 
        <!--     ============================== Fieldset 2 end ============================== -->

